# Copyright 2017, Dekoruma

# This software is provided as an interface
# to integrate Sketchup with Dekoruma Platform

require 'sketchup.rb'
require 'extensions.rb'

extension_name = "Dekoruma"

# Load plugin as extension (so that user can disable it)
d_extension = SketchupExtension.new(extension_name, "su_dekoruma_plugin")
d_extension.copyright = "2019, Dekoruma"
d_extension.creator = "Rama"
d_extension.version = "2.0.0"
d_extension.description = "Use Dekoruma plugins to integrate Sketchup with Dekoruma Platform"

# Register the extension with Sketchup
Sketchup.register_extension d_extension, true
