# Sketchup Dekoruma Plugin

This plugin is used as an interface to integrate Sketchup with Dekoruma Platform

## Installing Plugin

Copy plugin to Users/Library/Application Support/SketchUp 2017/SketchUp/Plugins/

Compressed Folder : su_dekoruma_plugin.rbz
- Loader File : su_dekoruma_plugin.rb
- Folder : su_dekoruma_plugin
    - Main File : su_dekoruma_plugin.rb
